# CalderaFormsCNPJValidator

Plugin do Wordpress para utilizar juntamento com o [Caldera Forms](https://calderaforms.com/).

Adiciona um Processador aos formulários que permite a validação de campos contendo CNPJ (Cadastro Nacional de Pessoas Juridicas).

Autor: Júnio Edurdo de Morais Aquino (eduardomorais211@gmail.com)
