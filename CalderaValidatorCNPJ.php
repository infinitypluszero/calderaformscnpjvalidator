<?php
/**
 * Plugin Name: Caldera Forms CNPJ Validator
 * Description: Adiciona um processador ao Caldera Forms que permite validar campos de CNPJ.
 * Version: 1.0
 * Author: Júnio Eduardo de Morais Aquino
 */


function validar_cnpj($cnpj) {  
    // Remove itens não númericos
    $cnpj = preg_replace('/[^0-9]/is', '', $cnpj);
    // Verifica se o número de digitos está correto
    if(strlen($cnpj) != 14) {
        return false;
    }
    // Verifica se o número é repetido
    if(preg_match('/(\d)\1{13}/', $cnpj)) {
        return false;
    }
    // Valida primeiro digito
    $soma = 0;
    for($i = 0, $j = 5; $i < 12; $i++) {
        $soma += $cnpj{$i} * $j;
        $j = ($j == 2) ? 9 : $j - 1;
    }

    $resto = $soma % 11;
    $resto = ($resto < 2) ? 0 : 11 - $resto;

    if($cnpj{12} != $resto) {
        return false;
    }
 
    // Valida segundo digito
    for($i = 0, $j = 6; $i < 13; $i++) {
        $soma += $cnpj{$i} * $j;
        $j = ($j == 2) ? 9 : $j - 1;
    }

    $resto = $soma % 11;
    $resto = ($resto < 2) ? 0 : 11 - $resto;

    return $cnpj{13} == $resto;
}

add_filter('caldera_forms_get_form_processors', 'caldera_cnpj_validator_processor');

/**
 * Adiciona um validador personalizado ao Caldera Forms que permite validar um campo de CNPJ.
 * 
 * @uses 'caldera_cnpj_validator_processor'
 * @param array $processors Processor configs
 * @return array
 */
function caldera_cnpj_validator_processor($processors) {
    $processors[''] = array(
        'name' => __('Validar CNPJ'),
        'description' => __('Valida um campo de CNPJ (Cadastro Nacional de Pessoa Jurídica)'),
        'pre_processor' => 'cnpj_validator',
        'template' => dirname(__FILE__) . '/config.php'
    );

    return $processors;
}

/**
 * Campos a serem utilizados pelo processador
 * @return array
 */
function caldera_cnpj_validator_fields() {
    return array(
        array(
            'id' => 'cnpj-field-to-validate',
            'type' => 'text',
            'required' => true,
            'magic' => true,
            'label' => __("Campo a validar")
        ),
    );
}

function cnpj_validator(array $config, array $form) {
    // Usa as APIs do Caldera Forms para obter o valor do campo que será validado
    $data = new Caldera_Forms_Processor_Get_Data($config, $form, caldera_cnpj_validator_fields());
    $value = $data->get_value('cnpj-field-to-validate');
    // Realiza validação, e em caso de erro retorna uma mensagem informando o erro
    if(validar_cnpj($value) == false) {
        // Obtém o ID do campo onde a mensagem de erro será exibida.
        $fields = $data->get_fields();
        $field_id = $fields['cnpj-field-to-validate']['config_field'];
        // Obtém a label do campo onde a mensagem de erro será exibida
        $field = $form['fields'][$field_id];
        $label = $field['label'];
        // Retorna a mensagem de erro
        return array(
            'type' => 'error',
            // Mensagem que será exibida no topo do formulário.
            'note' => sprintf(__('Por favor corrija o campo %s'), $label),
            'fields' => array(
                // Mensagem que será exibida no campo
                $field_id => __('O CNPJ informado é inválido')
            )
        );
    }
    // Em caso de sucesso, não retorna nada
}
